# Polymorphic future

Future implementation using a polymorphic proxy. The future result could be used 
as the original one.


## Requirements
* Python 3.7+

## Example

You can operate with future result as you operate with the integer

```python
>>> from future import Future
>>> result = Future(lambda: 1)
>>> result + 1
2
>>> result - 1
0

```

A more complex Example

```python
>>> import time
>>> import requests
>>>
>>> def get_example():
...   print("getting example")
...   time.sleep(10)
...   return requests.get("https://example.com").content
...
>>> future = Future(get_example)  # the function starts in background inmediatly
getting example
>>> future[:50]  # this blocks the execution
b'<!doctype html>\n<html>\n<head>\n    <title>Example D'

```
See it in action
![example.gif](docs/example.gif)
