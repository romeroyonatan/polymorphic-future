import datetime
import time
import unittest

from future import Future, CPUIntensiveFuture


class FutureTestCase(unittest.TestCase):
    def test_future_is_polymorphic_with_integers(self):
        future = Future(lambda: 1)
        self.assertEqual(future + 1, 2)
        self.assertEqual(future - 1, 0)
        self.assertEqual(future * 3, 3)
        self.assertEqual(3 + future, 4)

    def test_future_is_polymorphic_with_booleans(self):
        future = Future(lambda: True)
        self.assertTrue(future)

        future = Future(lambda: False)
        self.assertFalse(future)

    def test_attribute_accessing_is_forwarded_to_future_result(self):
        future = Future(lambda: datetime.date(1970, 1, 2))
        self.assertEqual(future.year, 1970)
        self.assertEqual(future.month, 1)
        self.assertEqual(future.day, 2)

    def test_futures_run_in_parallel(self):
        def test_2_milliseconds():
            future1 = Future(sleepy_echo("100 milliseconds", 100))
            future2 = Future(sleepy_echo("200 milliseconds", 200))
            return "{} {}".format(future1, future2)

        self.assert_runs_in_less_than(test_2_milliseconds, expected_milliseconds=210)

    def test_cpu_intensive_tasks_runs_faster_on_cpu_intensive_futures(self):
        tasks = [
            CPUIntensiveFuture(cpu_bound),
            CPUIntensiveFuture(cpu_bound),
            CPUIntensiveFuture(cpu_bound),
        ]
        result = " ".join(str(task) for task in tasks)

        self.assertEqual("OK OK OK", result)

    def test_exception_is_handled_on_the_caller(self):
        def fail():
            raise RuntimeError("Something goes wrong")

        future = Future(fail)

        with self.assertRaises(expected_exception=RuntimeError, msg="Something goes wrong"):
            future.value()

    def test_exception_is_handled_on_the_caller_on_cpu_intensive_applications(self):
        def fail():
            raise RuntimeError("Something goes wrong")

        future = CPUIntensiveFuture(fail)

        with self.assertRaises(expected_exception=RuntimeError, msg="Something goes wrong"):
            future.value()

    def assert_runs_in_less_than(self, a_closure, expected_milliseconds):
        duration = duration_in_ns(a_closure)
        self.assertLess(duration, expected_milliseconds * 1e6)


def sleepy_echo(a_string: str, sleep_milliseconds: float):
    def echo():
        time.sleep(sleep_milliseconds * 1e-3)
        return a_string

    return echo


def cpu_bound():
    i = 10_000_000
    while i:
        i = i - 1
    return "OK"


def duration_in_ns(a_closure):
    start = time.time_ns()
    a_closure()
    return time.time_ns() - start


