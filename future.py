import multiprocessing
import threading
from typing import TypeVar, Generic, Callable

T = TypeVar("T")


class DunderMethodsProxy(type):
    """Forward all magic methods to the class object.

    This is necessary because __getattribute__ does not catch calls to dunder methods.
    """

    DUNDERS = [
        "__abs__",
        "__add__",
        "__aiter__",
        "__and__",
        "__bool__",
        "__ceil__",
        "__complex__",
        "__contains__",
        "__delitem__",
        "__div__",
        "__divmod__",
        "__enter__",
        "__eq__",
        "__exit__",
        "__float__",
        "__floor__",
        "__floordiv__",
        "__fspath__",
        "__ge__",
        "__getitem__",
        "__gt__",
        "__hash__",
        "__iadd__",
        "__iand__",
        "__idiv__",
        "__ifloordiv__",
        "__ilshift__",
        "__imatmul__",
        "__imod__",
        "__imul__",
        "__index__",
        "__int__",
        "__invert__",
        "__ior__",
        "__ipow__",
        "__irshift__",
        "__isub__",
        "__iter__",
        "__itruediv__",
        "__ixor__",
        "__le__",
        "__len__",
        "__lshift__",
        "__lt__",
        "__matmul__",
        "__mod__",
        "__mul__",
        "__ne__",
        "__neg__",
        "__next__",
        "__or__",
        "__pos__",
        "__pow__",
        "__radd__",
        "__rand__",
        "__rdiv__",
        "__rdivmod__",
        "__rfloordiv__",
        "__rlshift__",
        "__rmatmul__",
        "__rmod__",
        "__rmul__",
        "__ror__",
        "__round__",
        "__rpow__",
        "__rrshift__",
        "__rshift__",
        "__rsub__",
        "__rtruediv__",
        "__rxor__",
        "__setitem__",
        "__sizeof__",
        "__str__",
        "__sub__",
        "__truediv__",
        "__trunc__",
        "__xor__",
    ]

    def __new__(cls, *args, **kwargs):
        a_class = super().__new__(cls, *args, **kwargs)
        for dunder in cls.DUNDERS:
            setattr(a_class, dunder, DunderMethodsProxy._forward(dunder))
        return a_class

    @classmethod
    def _forward(cls, message):
        def proxy(self, *args, **kwargs):
            return self.__getattr__(message)(*args, **kwargs)

        return proxy


class Future(Generic[T], metaclass=DunderMethodsProxy):
    """Polymorphic future result.

    Run the callable in background and get the result in the future. Because
    this future is a polymorphic proxy of future result, there are not need of
    call any other method.
    """

    _result: T

    def __init__(self, a_closure: Callable[[], T]):
        self._result = None
        self._exception = None

        def run():
            try:
                self._result = a_closure()
            except Exception as exception:
                self._exception = exception

        self._thread = threading.Thread(target=run)
        self._thread.start()

    def value(self) -> T:
        self._thread.join()
        if self._exception is not None:
            raise self._exception
        return self._result

    def __getattr__(self, item):
        return getattr(self.value(), item)


class CPUIntensiveFuture(Generic[T], metaclass=DunderMethodsProxy):
    """Polymorphic future result for CPU intensive tasks.

    Run the callable in background and get the result in the future. Because
    this future is a polymorphic proxy of future result, there are not need of
    call any other method.
    """

    UNSET = object()
    _result: T

    def __init__(self, a_closure: Callable[[], T]):
        self._result = self.UNSET
        self._exception = self.UNSET
        self._queue = multiprocessing.SimpleQueue()
        self._exception_queue = multiprocessing.SimpleQueue()

        def run():
            try:
                result = a_closure()
                self._queue.put(result)
            except Exception as exception:
                self._exception_queue.put(exception)

        self._process = multiprocessing.Process(target=run)
        self._process.start()

    def value(self) -> T:
        self._process.join()
        self._handle_errors()
        self._get_result()
        return self._result

    def _handle_errors(self):
        if self._exception is self.UNSET and not self._exception_queue.empty():
            self._exception = self._exception_queue.get()
        if self._exception is not self.UNSET:
            raise self._exception

    def _get_result(self):
        if self._result is self.UNSET:
            self._result = self._queue.get()

    def __getattr__(self, item):
        return getattr(self.value(), item)

    def __del__(self):
        # Avoid zombie process
        Future(self._process.join)
